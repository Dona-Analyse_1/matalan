@storelocator
Feature: Store locator
Background: 
Given I am on the homepage

Scenario: verifyStorelocatorValid
When I click on store finder
And enter BR1 2AH
And I click on Find
Then I should see stores in that location


Scenario: verifyStorelocatorInvalid
When I click on store finder
And enter BS1000 38S
And I click on Find
Then I should see Please enter a valid address, city or postcode message