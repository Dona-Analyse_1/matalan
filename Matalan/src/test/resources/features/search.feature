@regression @search
Feature: Search

Scenario Outline: verifyWithValid
Given I am on the homepage
When I enter "<ProductType>"
Then I should see "<RelatedProducts>"


Examples:
|ProductType|RelatedProducts|
|Blue|Search Results for 'blue' – Matalan|
|Jeans|Search Results for 'jeans' – Matalan|
|Shoes|Search Results for 'shoes' – Matalan|
|Shirt|Search Results for 'shirt' – Matalan|
|Womens|Womens Clothing & Fashion - Buy Ladies Clothes Online – Matalan|




Scenario Outline: verifyWithInvalid
Given I am on the homepage
When I enter "<ProductType>"
Then I should see "<RelatedText>"

Examples:
|ProductType|RelatedText|
|Pumpkin|Search Results for 'pumpkin' – Matalan|
|Goats|Search Results for 'goats' – Matalan|
|Rice|Search Results for 'rice' – Matalan|
|134789302|Search Results for '134789302' – Matalan|
| |Online Clothes Shopping - Shop Latest Fashion – Matalan|






