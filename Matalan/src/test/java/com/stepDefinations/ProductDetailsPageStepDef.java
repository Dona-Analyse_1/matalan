package com.stepDefinations;

import com.runner.BaseTest;

import com.pages.ProductDetailsPage;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;

public class ProductDetailsPageStepDef extends BaseTest {
	
	ProductDetailsPage productDetailsPage=new ProductDetailsPage ();

	@Then("^I should see product details page$")
	public void i_should_see_product_details_page() throws Throwable {
		productDetailsPage.verifyProductDetailsPage();
}

	@Then("^I should be able to click on Add to Bag button$")
	public void i_should_be_able_to_click_on_Add_to_Bag_button() throws Throwable {
	   productDetailsPage.verifyAddToBagButton();
	}
	
	@And("^I should be able to select the size$")
	public void i_should_be_able_to_select_the_size() throws Throwable {
		productDetailsPage.selectSizeSelector();
	}
	@And("^I should be able to select the quantity$")
	public void i_should_be_able_to_select_the_quantity () throws Throwable {
		productDetailsPage.verifyDressingGownQuantitySelector();
	}
	
	    
	}

	
    


