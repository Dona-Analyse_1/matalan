package com.stepDefinations;

import com.pages.HomePage;
import com.runner.BaseTest;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class HomePageStepDefination extends BaseTest{
	HomePage homePage=new HomePage();


	@Given("^I am on the homepage$")
	public void i_am_on_the_home_page() throws Throwable {
	    homePage.verifyHomePage();
}
@Given("^I click on My Account Link$")
public void i_click_on_My_Account_Link() throws Throwable {
   homePage.selectLoginLink();
}
@When("^I search for Socks$")
public void i_search_for_Socks() throws Throwable {
	homePage.searchForSocksProduct();
   }	
@When("^I search for Ladies Dressing Gown$")
public void i_search_for_Ladies_Dressing_Gown() throws Throwable {
	homePage.searchForLadiesDressingGownProduct();
}
@When("^I enter \"([^\"]*)\"$")
public void i_enter(String ProductType) throws Throwable {
	homePage.searchForTheProduct(ProductType);
}
@Then("^I should be taken to another page and see an error message$")
public void i_should_be_taken_to_another_page_and_see_an_error_message() throws Throwable {
}
@When("^I scroll down$")
public void i_scroll_down() throws Throwable {
    homePage.scrollDown();
}

@When("^click on Sign me up text box$")
public void click_on_Sign_me_up_text_box() throws Throwable {
	homePage.clickOnSignMeUpTextBox();
    
}
@Then ("^I should be able to enter dna.a@gmail.com$")
public void i_should_be_able_to_enter_dna_a_gmail_com() throws Throwable {
	homePage.enterValidEmailInSignMeUpTextBox();
}
@Then ("^I should be able to enter 113432@mail.bt$")
public void i_should_be_able_to_enter_113432_mail_bt ()throws Throwable {
	homePage.enterInvalidEmailSignMeUpTextBox();
}

@And("^I should be able to click on the sign me up button$")
public void i_should_be_able_to_click_on_the_sign_me_up_button() throws Throwable {
	homePage.clickOnSignMeUpButton();
 }

@And("^I should be able to click on checkout$")
public void i_should_be_able_to_click_on_checkout() throws Throwable {
	homePage.verifyCheckOutButton();
}
@When("^I click on store finder$")
public void i_click_on_store_finder() throws Throwable {
	homePage.selectStoreLocatorLink();
   
}
    
}





