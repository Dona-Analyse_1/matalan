package com.stepDefinations;

import cucumber.api.java.After;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseTest;

import cucumber.api.java.Before;

public class Hooks extends BaseTest {
	

	@Before
	public void start() {
		System.setProperty("webdriver.chrome.driver","C:/Users/Dona Analyse/eclipse-workspace/WebDriverWithJava/src/driver/chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://www.matalan.co.uk/");
		driver.manage().window().maximize();
		driver.findElement(By.cssSelector("button[aria-label='Close']")).click();
		
		
		
		
		Assert.assertEquals("https://www.matalan.co.uk/", driver.getCurrentUrl());
		
}
	
	@After
	public void close() {
		//driver.close();
	}


	}

