package com.stepDefinations;

import com.pages.CheckoutPage;
import com.runner.BaseTest;

import cucumber.api.java.en.Then;

public class CheckoutPageStepDefination extends BaseTest {
	CheckoutPage checkoutPage=new CheckoutPage();
	
	@Then ("^I should be able to select click and collect button$") 
		public void i_should_be_able_to_select_click_and_collect_button() throws Throwable {
		checkoutPage.selectClickAndCollectOption();
	}
	
}
