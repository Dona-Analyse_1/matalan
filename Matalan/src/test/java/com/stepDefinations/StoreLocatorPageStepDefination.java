package com.stepDefinations;

import com.pages.StoreLocatorPage;
import com.runner.BaseTest;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreLocatorPageStepDefination extends BaseTest{
	
	StoreLocatorPage storeLocatorPage=new StoreLocatorPage();
	

	@When("^enter BR(\\d+) (\\d+)AH$")
	public void enter_BR_AH(int arg1, int arg2) throws Throwable {
	  storeLocatorPage.enterValidPostCode();  
	}

	@When("^I click on Find$")
	public void i_click_on_Find() throws Throwable {
		storeLocatorPage.clickOnFindBox();
	    
	}

	@Then("^I should see stores in that location$")
	public void i_should_see_stores_in_that_location() throws Throwable {
	storeLocatorPage.verifyStoreLocationPage();
	    
	}

	@When("^enter BS(\\d+) (\\d+)S$")
	public void enter_BS_S(int arg1, int arg2) throws Throwable {
	    storeLocatorPage.enterInvalidPostCode();
	}

	@Then("^I should see Please enter a valid address, city or postcode message$")
	public void i_should_see_Please_enter_a_valid_address_city_or_postcode_message() throws Throwable {
	    storeLocatorPage.verifyErrorMessage();
	}

}
