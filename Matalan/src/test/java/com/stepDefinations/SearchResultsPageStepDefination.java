package com.stepDefinations;

import com.pages.SearchResultsPage;
import com.runner.BaseTest;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class SearchResultsPageStepDefination extends BaseTest {
	
	SearchResultsPage searchResultsPage = new SearchResultsPage();
	

@Given("^I am on the search results page$")
public void i_am_on_the_search_results_page(String ExpectedResult) throws Throwable {
	searchResultsPage.searchResultsPage(ExpectedResult);
	
	

}

@Then("^I should see Socks related search$")
public void i_should_see_Socks_related_search() throws Throwable {
 searchResultsPage.verifySearchResultsPage();   
}
@Then("^I select particular product$")
public void i_select_particular_product() throws Throwable {
	searchResultsPage.selectParticularProduct();
    
}
	
	@Then("^I should see related products displayed$")
	public void i_should_see_related_products_displayed(String RelatedProducts) throws Throwable {
		searchResultsPage.searchProductWithFacet(RelatedProducts);
		
		searchResultsPage.seeRelatedPage(RelatedProducts);
		
	  
	}
	@Then("^I should see \"([^\"]*)\"$")
	public void i_should_see(String RelatedProducts) throws Throwable {
		searchResultsPage.seeRelatedPage(RelatedProducts);
		
}
	@Then("^I should see dressing gown related search results page$")
	public void i_should_see_dressing_gown_related_search_results_page() throws Throwable{
		searchResultsPage.verifyLadiesDressingGownSearchResultsPage();
	}
	@And("^I select particular dressing gown product$")
	public void i_select_particular_dressing_gown_product() throws Throwable{
		searchResultsPage.selectLadiesDressingGownProduct();
	}
	
	
	@Then("^I should see related text displayed$")
	public void i_should_see_related_text_displayed(String RelatedText) throws Throwable {
		searchResultsPage.seeRelatedPage(RelatedText);
}
	@When("^I select \"([^\"]*)\"$")
	public void i_select(String ExpectedProduct) throws Throwable {
		searchResultsPage.seeRelatedPage(ExpectedProduct);
	    
}
}

