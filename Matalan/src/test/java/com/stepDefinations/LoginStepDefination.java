package com.stepDefinations;

import com.pages.LoginPage;
import com.runner.BaseTest;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class LoginStepDefination extends BaseTest {
	LoginPage loginPage=new LoginPage() ;
	
	@When("^I click on email address box$")
	public void i_click_on_email_address_box() throws Throwable {
		loginPage.clickEmailAddressBox();
	}
	@When("^enter \"([^\"]*)\"$")
	public void enter(String EmailType) throws Throwable {
    	loginPage.enterInEmailAddressBox("dna@gmail.com");
    }
	@When ("^I click on password box$")
	
	public void i_click_on_password_box() throws Throwable {
		loginPage.clickPasswordBox();
	}
    
    @Then("^I should be able to enter \"([^\"]*)\"$")
    public void i_should_be_able_to_enter(String  PasswordType) throws Throwable {
    	loginPage.enterInPasswordBox("sunshine2019");
        
    	
    }
    @Then("^I should be able to enter dna@gmail\\.com$")
    public void i_should_be_able_to_enter_dna_gmail_com() throws Throwable {
    	loginPage.enterInEmailAddressBox("dna@gmail.com");
    }  
    

    @Then("^I should be able to enter sunshine\\d+$")
    public void i_should_be_able_to_enter_sunshine_2019() throws Throwable {
    	loginPage.enterInPasswordBox("sunshine2019");
   
    }
    @Then("^I should be able to enter 113433@mail.com$")
    public void i_should_be_able_to_enter_113433_mail_com() throws Throwable {
    	loginPage.enterEmailBox("113433@mail.com");
        
    	
    }
    @Then("^I should be able to enter 34da97$")
    public void i_should_be_able_to_enter_34da97() throws Throwable {
    	loginPage.enterPasswordBox();
    
    }
    @When("^I click on Email Address Register box$")
    public void i_click_on_Email_Address_Register_box() throws Throwable {
    	loginPage.clickEmailAddressBox();
    }
    @When("^enter email address da123@gmail.com$")
    public void enter_email_address_da_gmail_com() throws Throwable {
    	loginPage.enterRegistrationEmailAddressBox("da123@gmail.com");
        
    }

    @Then("^I should be able to click on Continue Securely$")
    public void i_should_be_able_to_click_on_Continue_Securely() throws Throwable {
    	loginPage.clickContinueSecurelyButton();
       
    }
   @When("^I enter email address dna@gmail.com$")
    public void i_enter_email_address_dna_gmail_com() throws Throwable {
	   loginPage.enterEmailAddress();
	   
   }
   @Then("^I should be able to click on Forgotten Password button$")
   public void i_should_be_able_to_click_on_forgotten_password_button() throws Throwable {
	   loginPage.clickOnForgotMyPasswordButton();
   }
    }
       
    
   
        
    


