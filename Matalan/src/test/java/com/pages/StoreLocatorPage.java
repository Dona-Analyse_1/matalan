package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseTest;









public class StoreLocatorPage extends BaseTest{
	private static By STOREFINDERTEXTBOX=By.cssSelector("input#addressEntry.o-input.o-input--textual.input-addon--expand.disable-enter");
    private static By FINDBOX=By.cssSelector("button#findStore.c-btn.c-btn--secondary.js-find-store");
    private static By ERRORMESSAGE=By.cssSelector("label.o-form__label--message");
  public void enterValidPostCode() {
	  driver.findElement(STOREFINDERTEXTBOX).click();
	  driver.findElement(STOREFINDERTEXTBOX).clear();
	  driver.findElement(STOREFINDERTEXTBOX).sendKeys("BR1 2AH");
  }
  public void clickOnFindBox() {
	  driver.findElement(FINDBOX).click();
  }
  public void enterInvalidPostCode() {
	  driver.findElement(STOREFINDERTEXTBOX).click();
	  driver.findElement(STOREFINDERTEXTBOX).clear();
	  driver.findElement(STOREFINDERTEXTBOX).sendKeys("BS1000 38S");
	  driver.findElement(FINDBOX).click();
	  
  }
  public void clickFindBoxInvalid() {
	  driver.findElement(FINDBOX).click();
  }
  public void verifyStoreLocationPage() {
	  driver.get("https://www.matalan.co.uk/stores");
  }
  public void verifyErrorMessage() {
	  Assert.assertEquals(ERRORMESSAGE, driver.getPageSource());
  }
}
