package com.pages;

import org.openqa.selenium.By;

import com.runner.BaseTest;

import org.junit.Assert;

public class SearchResultsPage extends BaseTest {
	
	
	
	private static By WOMENCATEGORY=By.cssSelector ("a#accessible-menu-1620854689200-1");
	
	private static By SHOPDRESSESBUTTON=By.cssSelector("a.c-btn.c-btn--clear.c-btn--large.u-mar-l-0.js-page-component-min-width.cta_14_2_1");
    private static By SEARCHRESULTPAGE=By.cssSelector("h1.o-product-detail__title");
    private static By ANIMALPRINTDRESSINGGOWNPRODUCT=By.cssSelector("img[alt='Soft Touch Animal Print Hooded Dressing Gown']");
  public void searchProductWithFacet (String RelatedProducts) {
	  
	  driver.findElement(WOMENCATEGORY).click();
	  driver.findElement(SHOPDRESSESBUTTON).click();
	  driver.findElement(SEARCHRESULTPAGE).getText();
	  
	 
	  Assert.assertEquals(RelatedProducts, driver.getClass());
	  
	 
}
  public void seeRelatedPage(String RelatedProduct) {
	  Assert.assertEquals(RelatedProduct, driver.getTitle());
  }

	public void searchRelatedPage(String RelatedText) {
		Assert.assertEquals(RelatedText, driver.getTitle());
		
	}
	
	public void searchResultsPage(String ExpectedResults) {
		Assert.assertEquals("5 Pack Glitter Heel & Toe"+ExpectedResults, driver.findElement(By.cssSelector("h1.o-product-detail__title")).getText());
	}
	public void verifySearchResultsPage() {
		Assert.assertEquals("Search Results for 'socks' – Matalan", driver.getTitle());
	}
	public void selectParticularProduct() {
		driver.findElement(By.cssSelector("img[alt='5 Pack Glitter Heel & Toe Socks']")).click();
	}
	public void verifyLadiesDressingGownSearchResultsPage() {
		Assert.assertEquals("Search Results for 'ladies dressing gown' – Matalan", driver.getTitle());
	}
	public void verifyLadiesDressingGownProduct() {
		driver.findElement(ANIMALPRINTDRESSINGGOWNPRODUCT).click();
	}
	public void selectLadiesDressingGownProduct() {
		driver.findElement(By.cssSelector("img[alt='Soft Touch Animal Print Hooded Dressing Gown']")).click();
	}
	}
	
		
	
	    

  
  









	
	
