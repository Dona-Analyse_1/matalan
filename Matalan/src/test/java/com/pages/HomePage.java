package com.pages;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;


import com.runner.BaseTest;

public class HomePage extends BaseTest  {
	
	
	
	private static By SEARCHTEXTBOX=By.cssSelector("input[analytics_re='Header-_-Search-_-Input']");
	private static By SEARCHBUTTON=By.xpath("//div[2]//form//button");
	private static By MYACCOUNTLINK=By.cssSelector("li[data-activity='link_my_account']");
	private static By SIGNUPTEXTBOX=By.cssSelector("input#newsletter_email.o-input.o-input--textual.input-addon--expand.js-exclude-unsaved");
	private static By SIGNMEUPBUTTON=By.cssSelector("button[data-action='subscribe']");
	private static By WOMENCATEGORYBUTTON=By.cssSelector("a#accessible-menu-1622288125979-1.sub-nav-inner.sub-nav-inner--link.js-navbar-sub-inner-link");
	private static By CHECKOUTBUTTON=By.cssSelector("a[analytics_re='Header-_-Action Bar-_-Checkout']");	
	private static By STOREFINDERLINK=By.linkText("stores");
	
	public void searchForTheProduct(String ProductType) {
		driver.findElement(SEARCHTEXTBOX).click();
		driver.findElement(SEARCHTEXTBOX).clear();
		driver.findElement(SEARCHTEXTBOX).sendKeys(ProductType);
		driver.findElements(SEARCHBUTTON).get(0).click();
		
	}
		
		public void verifyHomePage() {
		Assert.assertEquals("https://www.matalan.co.uk/", driver.getCurrentUrl());
		
	}
		public void verifyCheckOutButton() {
			driver.findElement(CHECKOUTBUTTON).click();
		}
 
		public void searchForProduct(String ExpectedProduct) {
			driver.findElement(SEARCHTEXTBOX).click();
			driver.findElement(SEARCHTEXTBOX).clear();
			driver.findElement(SEARCHTEXTBOX).sendKeys(ExpectedProduct);
			driver.findElement(WOMENCATEGORYBUTTON).click();
		}
		
		public void searchForSocksProduct () {
			driver.findElement(SEARCHTEXTBOX).click();
			driver.findElement(SEARCHTEXTBOX).clear();
			driver.findElement(SEARCHTEXTBOX).sendKeys("Socks");
			driver.findElements(SEARCHBUTTON).get(0).click();
		
		}
		public void selectLoginLink () {
			driver.findElement(MYACCOUNTLINK).click();
		}
		public void signUpValid() {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,2500)");
			driver.findElement(SIGNUPTEXTBOX).click();
			driver.findElement(SIGNUPTEXTBOX).clear();
			driver.findElement(SIGNUPTEXTBOX).sendKeys("dna.a@gmail.com");
			driver.findElement(SIGNMEUPBUTTON).click();
		}
		public void scrollDown() {
			JavascriptExecutor jse = (JavascriptExecutor)driver;
			jse.executeScript("window.scrollBy(0,2500)");
		}
		public void clickOnSignMeUpTextBox() {
			driver.findElement(By.cssSelector("input#newsletter_email.o-input.o-input--textual.input-addon--expand.js-exclude-unsaved")).click();
			
		}
		public void enterValidEmailInSignMeUpTextBox() {
			driver.findElement(By.cssSelector("input#newsletter_email.o-input.o-input--textual.input-addon--expand.js-exclude-unsaved")).sendKeys("dna.a@gmail.com");
			
		}
		public void enterInvalidEmailSignMeUpTextBox() {
			driver.findElement(By.cssSelector("input#newsletter_email.o-input.o-input--textual.input-addon--expand.js-exclude-unsaved")).sendKeys("113432@mail.bt");
		}
		public void clickOnSignMeUpButton() {
			driver.findElement(By.cssSelector("button[data-action='subscribe']")).click();
			
		}
		public void searchForLadiesDressingGownProduct() {
			driver.findElement(SEARCHTEXTBOX).clear();
			driver.findElement(SEARCHTEXTBOX).sendKeys("Ladies Dressing Gown");
			driver.findElement(SEARCHBUTTON).click();
			
		}
		public void selectStoreLocatorLink() {
			driver.findElement(STOREFINDERLINK).click();
			
		}
		 
}
