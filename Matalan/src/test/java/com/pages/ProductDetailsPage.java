package com.pages;




import org.junit.Assert;
import org.openqa.selenium.By;


import com.runner.BaseTest;


public class ProductDetailsPage extends BaseTest{
	
private static By SIZESELECTOR=By.cssSelector("a.selectBox.js-product-select--size.o-input.selectBox-dropdown");
private static By SIZESMALL=By.cssSelector("span[style='width: 100%;']");

private static By QUANTITYSELECTOR=By.cssSelector("span[style='width: 152.797px;']");
private static By QUANTTYNUMBER2SELECTOR=By.cssSelector("a[rel='2']");
private static By ADDTOBAGBUTTON =By.cssSelector("button#gtm-add");

public void AddToBasket(String expectedResult) throws InterruptedException {
	
	
		
driver.get("https://www.matalan.co.uk/product/detail/s2800361_c101/5-pack-glitter-heel-toe-socks-black");

driver.findElement(QUANTITYSELECTOR).click();
		
driver.findElement(ADDTOBAGBUTTON).click();
		
	}

public void verifySizeSmallSelector() {
	Assert.assertEquals("span[style='width: 100%;']", driver.findElement(SIZESMALL));
}
public void selectSizeSmall() {
	driver.findElement(SIZESMALL).click();
}
	
public void verifyQuantitySelector() {
			Assert.assertEquals("span[style='width: 152.797px;']", driver.findElement(QUANTITYSELECTOR));
	}
	
public void verifyAddToBagButton() {
	driver.findElement(ADDTOBAGBUTTON).click();
}
public void verifyProductDetailsPage() {
		
Assert.assertEquals("https://www.matalan.co.uk/product/detail/s2800361_c101/5-pack-glitter-heel-toe-socks-black",driver.getCurrentUrl());
}
public void verifyViewBasketLink() {
Assert.assertEquals("https://www.matalan.co.uk/cart", driver.getCurrentUrl());
	}
	public void verifyLadiesDressingGownProductDetailsPage() {
		Assert.assertEquals("https://www.matalan.co.uk/product/detail/s2811409_c124/soft-touch-animal-print-hooded-dressing-gown-aqua", driver.getCurrentUrl());
	
     }
	public void verifySizeSelector() {
		driver.findElement(SIZESELECTOR).click();
	}
	public void clickSizeSelector() {
		driver.findElement(SIZESELECTOR).click();
	}
	public void selectSizeSelector() {
		driver.findElement(By.cssSelector("a.selectBox.js-product-select--size.o-input.selectBox-dropdown")).click();
	}
	public void verifyDressingGownQuantitySelector() {
		driver.findElement(QUANTTYNUMBER2SELECTOR).click();
	}
	
	
}
