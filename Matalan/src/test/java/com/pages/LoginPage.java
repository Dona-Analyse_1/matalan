package com.pages;


import org.openqa.selenium.By;
import com.runner.BaseTest;


public class LoginPage extends BaseTest{
	
	private static By EMAILADDRESSBOX=By.cssSelector("input#account_email.o-input.o-input--textual.js-exclude-unsaved");
		
	private static By PASSWORDBOX=By.cssSelector("input#account_password.o-input.o-input--textual.js-exclude-unsaved");
	
	private static By REGISTRATIONEMAILADDRESSBOX=By.cssSelector("input#account_form_email.o-input.o-input--textual.js-exclude-unsaved");
	private static By FORGOTTENMYPASSWORDBUTTON=By.cssSelector("a.o-login__password-forgotten.o-form__label-option.link--text.u-mar-t-medium.u-font-ta-c");
	
	private static By CONTINUESECURELYBUTTON=By.cssSelector("button[data-behavior='save_button']");
	
	public void loginValid(CharSequence EmailType, CharSequence PasswordType) {
		
		driver.findElement(EMAILADDRESSBOX).click();
		driver.findElement(EMAILADDRESSBOX).clear();
		driver.findElement(EMAILADDRESSBOX).sendKeys("dna@gmail.com");;
		driver.findElement(PASSWORDBOX).click();
		driver.findElement(PASSWORDBOX).clear();
		driver.findElement(PASSWORDBOX).sendKeys("sunshine2019");
		
		}
	public void clickEmailAddressBox() {
		driver.findElement(By.cssSelector("input#account_email.o-input.o-input--textual.js-exclude-unsaved")).click();
	}
	public void clearEmailAddressBox() {
		driver.findElement(By.cssSelector("input#account_email.o-input.o-input--textual.js-exclude-unsaved")).clear();
	}
	
	public void enterInEmailAddressBox(String emailType) {
		
		driver.findElement(By.cssSelector("input#account_email.o-input.o-input--textual.js-exclude-unsaved")).sendKeys("dna@gmail.com");
		
	}
	public void clickPasswordBox() {
		driver.findElement(By.cssSelector("input#account_password.o-input.o-input--textual.js-exclude-unsaved")).click();
	}
	public void clearPasswordBox() {
		driver.findElement(By.cssSelector("input#account_password.o-input.o-input--textual.js-exclude-unsaved")).clear();
	}
	public void enterInPasswordBox(String passwordType) {
		
		driver.findElement(By.cssSelector("input#account_password.o-input.o-input--textual.js-exclude-unsaved")).sendKeys("sunshine2019");
	}
	public void LoginInvalid() {
		driver.findElement(EMAILADDRESSBOX).click();
		driver.findElement(EMAILADDRESSBOX).clear();
		driver.findElement(EMAILADDRESSBOX).sendKeys("113433@mail.com");
		driver.findElement(PASSWORDBOX).click();
		driver.findElement(PASSWORDBOX).clear();
		driver.findElement(PASSWORDBOX).sendKeys("34da97");
		
	}
	public void enterEmailBox(String string) {
	driver.findElement(By.cssSelector("input#account_email.o-input.o-input--textual.js-exclude-unsaved")).sendKeys("113433@mail.com");
}
	public void enterPasswordBox() {
		driver.findElement(By.cssSelector("input#account_password.o-input.o-input--textual.js-exclude-unsaved")).sendKeys("34da97");;
		
	}
	
	public void Register(String string) {
		driver.findElement(REGISTRATIONEMAILADDRESSBOX).click();
		driver.findElement(REGISTRATIONEMAILADDRESSBOX).clear();
		driver.findElement(REGISTRATIONEMAILADDRESSBOX).sendKeys("da123@gmail.com");
		driver.findElement(CONTINUESECURELYBUTTON).click();
		
}
	public void clickRegistrationEmailAddressBox() {
		driver.findElement(By.cssSelector("input#account_form_email.o-input.o-input--textual.js-exclude-unsaved")).click();
	}
	public void enterRegistrationEmailAddressBox(String string) {
		
		driver.findElement(By.cssSelector("input#account_form_email.o-input.o-input--textual.js-exclude-unsaved")).sendKeys("da123@gmail.com");
	}
	public void clickContinueSecurelyButton() {
		driver.findElement(By.cssSelector("button[data-behavior='save_button']")).click();
	}
	public void forgotMyPassword() {
		driver.findElement(EMAILADDRESSBOX).click();
		driver.findElement(EMAILADDRESSBOX).sendKeys("dna@gmail.com");
		driver.findElement(FORGOTTENMYPASSWORDBUTTON).click();
	}
	public void clickOnEmailAddressBox() {
		driver.findElement(By.cssSelector("input#account_email.o-input.o-input--textual.js-exclude-unsaved")).click();
	}
	public void enterEmailAddress() {
		driver.findElement(By.cssSelector("input#account_email.o-input.o-input--textual.js-exclude-unsaved")).sendKeys("dna@gmail.com");
	}
	public void clickOnForgotMyPasswordButton() {
		driver.findElement(By.cssSelector("a.o-login__password-forgotten.o-form__label-option.link--text.u-mar-t-medium.u-font-ta-c")).click();
	}
}
	
	



