package com.pages;


import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseTest;



public class CheckoutPage extends BaseTest{
	
	private static By CLICKANDCOLLECTBUTTON=By.cssSelector("input#click_and_collect.c-checkout-tappables__input");
	
	public void verifyCheckoutPage() {
		Assert.assertEquals("Checkout – Matalan", driver.getTitle());
	}
	public void selectClickAndCollectOption() {
		driver.findElement(CLICKANDCOLLECTBUTTON).click();
	}


}
