package com.runner;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;
@RunWith(Cucumber.class)


@CucumberOptions (features = "src/test/resources/features",
					 glue = {"com.stepDefinations"},
					 plugin = {"html:target/cucumber-html-report", "json:target/cucumber.json"},
                    tags="@storelocator"
		
		
		
		)

public class CucumberRunner {

}
